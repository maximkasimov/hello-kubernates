#!/usr/env bash

ansible -i /vagrant/kubernetes-setup/inventory/hosts.yml masters --ask-pass --user=vagrant  -m ping
ansible -i /vagrant/kubernetes-setup/inventory/hosts.yml nodes --ask-pass --user=vagrant  -m ping

ansible-playbook -i /vagrant/kubernetes-setup/inventory/hosts.yml  --ask-pass --user=vagrant --extra-vars=node_ip="192.168.50.10" /vagrant/kubernetes-setup/master-playbook.yml
ansible-playbook -i /vagrant/kubernetes-setup/inventory/hosts.yml  --ask-pass --user=vagrant --extra-vars=node_ip="192.168.50.11" /vagrant/kubernetes-setup/node-playbook.yml
ansible-playbook -i /vagrant/kubernetes-setup/inventory/hosts.yml  --ask-pass --user=vagrant --extra-vars=node_ip="192.168.50.12" /vagrant/kubernetes-setup/node-playbook.yml

# to get nodes to ready status
# You have to install network pod on your cluster. Below command install weave:
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"